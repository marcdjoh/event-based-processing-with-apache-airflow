from airflow.sensors.base import BaseSensorOperator
from airflow.providers.google.cloud.hooks.gcs import GCSHook


class MultipleGcsBucketSensor(BaseSensorOperator):
    template_fields = (
        'buckets',
        'impersonation_chain',
    )

    def __init__(
        self,
        *,
        buckets,
        google_cloud_conn_id = 'google_cloud_default',
        delegate_to = None,
        impersonation_chain = None,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.buckets = buckets
        self.google_cloud_conn_id = google_cloud_conn_id
        self.delegate_to = delegate_to
        self.impersonation_chain = impersonation_chain
        self._matches = []

    def poke(self, context):
        hook = GCSHook(
            gcp_conn_id=self.google_cloud_conn_id,
            delegate_to=self.delegate_to,
            impersonation_chain=self.impersonation_chain,
        )
        for bucket in self.buckets:
            match = [{'bucket': bucket, 'object': object} for object in hook.list(bucket)]
            self._matches.extend(match)
        return bool(self._matches)

    def execute(self, context):
        super().execute(context)
        return self._matches