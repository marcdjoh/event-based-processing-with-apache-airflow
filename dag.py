from datetime import datetime, timedelta

from airflow.decorators import dag, task
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from multiple_gcs_bucket_sensor import MultipleGcsBucketSensor

from google.cloud import storage


def process_objects(**kwargs):
    ti = kwargs['ti']
    objects = ti.xcom_pull(task_ids='wait_for_objects', key='return_value')
    storage_client = storage.Client()
    for element in objects:
        bucket = storage_client.bucket(element['bucket'])
        blob = bucket.blob(element['object'])
        blob.delete()

@dag(start_date=datetime(2021, 1, 1),
     schedule_interval='@once', catchup=False)
def files_processor():

    wait_for_objects_task = MultipleGcsBucketSensor(
        task_id='wait_for_objects',
        buckets=['sandbox-mdjohossou-first-bucket', 'sandbox-mdjohossou-second-bucket']
    )

    process_objects_task = PythonOperator(
        task_id='process_objects',
        python_callable=process_objects,
        provide_context=True
    )

    trigger_files_processor_dag_task = TriggerDagRunOperator(
        task_id='trigger_files_processor_dag',
        trigger_dag_id='files_processor'
    )

    wait_for_objects_task >> process_objects_task >> trigger_files_processor_dag_task

dag = files_processor()